from flask import Flask, jsonify

app = Flask(__name__)


@app.route('/')
def index():
    return 'eBot API'


@app.route('/scores')
def hello_world():
    return 'Scores API'
