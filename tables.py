# coding: utf-8
from sqlalchemy import BigInteger, Column, DateTime, Float, ForeignKey, Integer, SmallInteger, String, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base


Base = declarative_base()
metadata = Base.metadata


class Advertising(Base):
    __tablename__ = 'advertising'

    id = Column(BigInteger, primary_key=True)
    season_id = Column(ForeignKey(u'seasons.id', ondelete=u'CASCADE'), index=True)
    message = Column(Text)
    active = Column(Integer)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    season = relationship(u'Season')


class Config(Base):
    __tablename__ = 'configs'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(50))
    description = Column(String(255))
    content = Column(Text)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class Map(Base):
    __tablename__ = 'maps'

    id = Column(BigInteger, primary_key=True)
    match_id = Column(ForeignKey(u'matchs.id', ondelete=u'CASCADE'), nullable=False, index=True)
    map_name = Column(String(50))
    score_1 = Column(BigInteger)
    score_2 = Column(BigInteger)
    current_side = Column(String(255))
    status = Column(Integer)
    maps_for = Column(String(255))
    nb_ot = Column(BigInteger)
    identifier_id = Column(BigInteger)
    tv_record_file = Column(String(255))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    match = relationship(u'Match', primaryjoin='Map.match_id == Match.id')


class MapsScore(Base):
    __tablename__ = 'maps_score'

    id = Column(BigInteger, primary_key=True)
    map_id = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), nullable=False, index=True)
    type_score = Column(String(255))
    score1_side1 = Column(BigInteger)
    score1_side2 = Column(BigInteger)
    score2_side1 = Column(BigInteger)
    score2_side2 = Column(BigInteger)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    map = relationship(u'Map')


class Match(Base):
    __tablename__ = 'matchs'

    id = Column(BigInteger, primary_key=True)
    ip = Column(String(50))
    server_id = Column(ForeignKey(u'servers.id', ondelete=u'SET NULL'), index=True)
    season_id = Column(ForeignKey(u'seasons.id', ondelete=u'SET NULL'), index=True)
    team_a = Column(ForeignKey(u'teams.id', ondelete=u'SET NULL'), index=True)
    team_a_flag = Column(String(2))
    team_a_name = Column(String(25))
    team_b = Column(ForeignKey(u'teams.id', ondelete=u'SET NULL'), index=True)
    team_b_flag = Column(String(2))
    team_b_name = Column(String(25))
    status = Column(SmallInteger)
    is_paused = Column(Integer)
    score_a = Column(BigInteger)
    score_b = Column(BigInteger)
    max_round = Column(Integer, nullable=False)
    rules = Column(String(200), nullable=False)
    overtime_startmoney = Column(BigInteger)
    overtime_max_round = Column(Integer)
    config_full_score = Column(Integer)
    config_ot = Column(Integer)
    config_streamer = Column(Integer)
    config_knife_round = Column(Integer)
    config_switch_auto = Column(Integer)
    config_auto_change_password = Column(Integer)
    config_password = Column(String(50))
    config_heatmap = Column(Integer)
    config_authkey = Column(String(200))
    enable = Column(Integer)
    map_selection_mode = Column(String(255))
    ingame_enable = Column(Integer)
    current_map = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), index=True)
    force_zoom_match = Column(Integer)
    identifier_id = Column(String(100))
    startdate = Column(DateTime)
    auto_start = Column(Integer)
    auto_start_time = Column(Integer)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    map = relationship(u'Map', primaryjoin='Match.current_map == Map.id')
    season = relationship(u'Season')
    server = relationship(u'Server')
    team = relationship(u'Team', primaryjoin='Match.team_a == Team.id')
    team1 = relationship(u'Team', primaryjoin='Match.team_b == Team.id')

    def to_json(self):
        return dict(team_a=self.team_a_name,
                    team_a_score=self.score_a,
                    team_b=self.team_b_name,
                    team_b_score=self.score_b,)


class PlayerKill(Base):
    __tablename__ = 'player_kill'

    id = Column(BigInteger, primary_key=True)
    match_id = Column(ForeignKey(u'matchs.id', ondelete=u'CASCADE'), nullable=False, index=True)
    map_id = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), nullable=False, index=True)
    killer_name = Column(String(100))
    killer_id = Column(ForeignKey(u'players.id', ondelete=u'CASCADE'), index=True)
    killer_team = Column(String(20))
    killed_name = Column(String(100))
    killed_id = Column(ForeignKey(u'players.id', ondelete=u'CASCADE'), index=True)
    killed_team = Column(String(20))
    weapon = Column(String(100))
    headshot = Column(Integer)
    round_id = Column(BigInteger)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    killed = relationship(u'Player', primaryjoin='PlayerKill.killed_id == Player.id')
    killer = relationship(u'Player', primaryjoin='PlayerKill.killer_id == Player.id')
    map = relationship(u'Map')
    match = relationship(u'Match')


class Player(Base):
    __tablename__ = 'players'

    id = Column(BigInteger, primary_key=True)
    match_id = Column(ForeignKey(u'matchs.id', ondelete=u'CASCADE'), nullable=False, index=True)
    map_id = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), nullable=False, index=True)
    player_key = Column(String(255))
    team = Column(String(255), server_default=text("'other'"))
    ip = Column(String(255))
    steamid = Column(String(255))
    first_side = Column(String(255))
    current_side = Column(String(255))
    pseudo = Column(String(255))
    nb_kill = Column(BigInteger, server_default=text("'0'"))
    assist = Column(BigInteger, server_default=text("'0'"))
    death = Column(BigInteger, server_default=text("'0'"))
    point = Column(BigInteger, server_default=text("'0'"))
    hs = Column(BigInteger, server_default=text("'0'"))
    defuse = Column(BigInteger, server_default=text("'0'"))
    bombe = Column(BigInteger, server_default=text("'0'"))
    tk = Column(BigInteger, server_default=text("'0'"))
    nb1 = Column(BigInteger, server_default=text("'0'"))
    nb2 = Column(BigInteger, server_default=text("'0'"))
    nb3 = Column(BigInteger, server_default=text("'0'"))
    nb4 = Column(BigInteger, server_default=text("'0'"))
    nb5 = Column(BigInteger, server_default=text("'0'"))
    nb1kill = Column(BigInteger, server_default=text("'0'"))
    nb2kill = Column(BigInteger, server_default=text("'0'"))
    nb3kill = Column(BigInteger, server_default=text("'0'"))
    nb4kill = Column(BigInteger, server_default=text("'0'"))
    nb5kill = Column(BigInteger, server_default=text("'0'"))
    pluskill = Column(BigInteger, server_default=text("'0'"))
    firstkill = Column(BigInteger, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    map = relationship(u'Map')
    match = relationship(u'Match')


class PlayersHeatmap(Base):
    __tablename__ = 'players_heatmap'

    id = Column(BigInteger, primary_key=True)
    match_id = Column(ForeignKey(u'matchs.id', ondelete=u'CASCADE'), nullable=False, index=True)
    map_id = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), nullable=False, index=True)
    event_name = Column(String(50))
    event_x = Column(Float(18, True))
    event_y = Column(Float(18, True))
    event_z = Column(Float(18, True))
    player_name = Column(String(255))
    player_id = Column(ForeignKey(u'players.id', ondelete=u'CASCADE'), index=True)
    player_team = Column(String(20))
    attacker_x = Column(Float(18, True))
    attacker_y = Column(Float(18, True))
    attacker_z = Column(Float(18, True))
    attacker_name = Column(String(255))
    attacker_id = Column(ForeignKey(u'players.id', ondelete=u'SET NULL'), index=True)
    attacker_team = Column(String(20))
    round_id = Column(BigInteger)
    round_time = Column(BigInteger)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    attacker = relationship(u'Player', primaryjoin='PlayersHeatmap.attacker_id == Player.id')
    map = relationship(u'Map')
    match = relationship(u'Match')
    player = relationship(u'Player', primaryjoin='PlayersHeatmap.player_id == Player.id')


class PlayersSnapshot(Base):
    __tablename__ = 'players_snapshot'

    id = Column(BigInteger, primary_key=True)
    player_id = Column(ForeignKey(u'players.id', ondelete=u'CASCADE'), nullable=False, index=True)
    player_key = Column(String(255))
    first_side = Column(String(255))
    current_side = Column(String(255))
    nb_kill = Column(BigInteger, server_default=text("'0'"))
    assist = Column(BigInteger, server_default=text("'0'"))
    death = Column(BigInteger, server_default=text("'0'"))
    point = Column(BigInteger, server_default=text("'0'"))
    hs = Column(BigInteger, server_default=text("'0'"))
    defuse = Column(BigInteger, server_default=text("'0'"))
    bombe = Column(BigInteger, server_default=text("'0'"))
    tk = Column(BigInteger, server_default=text("'0'"))
    nb1 = Column(BigInteger, server_default=text("'0'"))
    nb2 = Column(BigInteger, server_default=text("'0'"))
    nb3 = Column(BigInteger, server_default=text("'0'"))
    nb4 = Column(BigInteger, server_default=text("'0'"))
    nb5 = Column(BigInteger, server_default=text("'0'"))
    nb1kill = Column(BigInteger, server_default=text("'0'"))
    nb2kill = Column(BigInteger, server_default=text("'0'"))
    nb3kill = Column(BigInteger, server_default=text("'0'"))
    nb4kill = Column(BigInteger, server_default=text("'0'"))
    nb5kill = Column(BigInteger, server_default=text("'0'"))
    pluskill = Column(BigInteger, server_default=text("'0'"))
    firstkill = Column(BigInteger, server_default=text("'0'"))
    round_id = Column(BigInteger)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    player = relationship(u'Player')


class Round(Base):
    __tablename__ = 'round'

    id = Column(BigInteger, primary_key=True)
    match_id = Column(ForeignKey(u'matchs.id', ondelete=u'CASCADE'), nullable=False, index=True)
    map_id = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), nullable=False, index=True)
    event_name = Column(String(255))
    event_text = Column(Text)
    event_time = Column(BigInteger)
    kill_id = Column(ForeignKey(u'player_kill.id', ondelete=u'SET NULL'), index=True)
    round_id = Column(BigInteger)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    kill = relationship(u'PlayerKill')
    map = relationship(u'Map')
    match = relationship(u'Match')


class RoundSummary(Base):
    __tablename__ = 'round_summary'

    id = Column(BigInteger, primary_key=True)
    match_id = Column(ForeignKey(u'matchs.id', ondelete=u'CASCADE'), nullable=False, index=True)
    map_id = Column(ForeignKey(u'maps.id', ondelete=u'CASCADE'), nullable=False, index=True)
    bomb_planted = Column(Integer)
    bomb_defused = Column(Integer)
    bomb_exploded = Column(Integer)
    win_type = Column(String(255), server_default=text("'normal'"))
    team_win = Column(String(255))
    ct_win = Column(Integer)
    t_win = Column(Integer)
    score_a = Column(SmallInteger)
    score_b = Column(SmallInteger)
    best_killer = Column(ForeignKey(u'players.id', ondelete=u'SET NULL'), index=True)
    best_killer_nb = Column(BigInteger)
    best_killer_fk = Column(Integer)
    best_action_type = Column(Text)
    best_action_param = Column(Text)
    backup_file_name = Column(String(255))
    round_id = Column(BigInteger)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    player = relationship(u'Player')
    map = relationship(u'Map')
    match = relationship(u'Match')


class Season(Base):
    __tablename__ = 'seasons'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(50), nullable=False)
    event = Column(String(50), nullable=False)
    start = Column(DateTime, nullable=False)
    end = Column(DateTime, nullable=False)
    link = Column(String(100))
    logo = Column(String(255))
    active = Column(Integer)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class Server(Base):
    __tablename__ = 'servers'

    id = Column(BigInteger, primary_key=True)
    ip = Column(String(50), nullable=False)
    rcon = Column(String(50), nullable=False)
    hostname = Column(String(100), nullable=False)
    tv_ip = Column(String(100))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class SfGuardForgotPassword(Base):
    __tablename__ = 'sf_guard_forgot_password'

    id = Column(BigInteger, primary_key=True)
    user_id = Column(ForeignKey(u'sf_guard_user.id', ondelete=u'CASCADE'), nullable=False, index=True)
    unique_key = Column(String(255))
    expires_at = Column(DateTime, nullable=False)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    user = relationship(u'SfGuardUser')


class SfGuardGroup(Base):
    __tablename__ = 'sf_guard_group'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(255), unique=True)
    description = Column(Text)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class SfGuardGroupPermission(Base):
    __tablename__ = 'sf_guard_group_permission'

    group_id = Column(ForeignKey(u'sf_guard_group.id', ondelete=u'CASCADE'), primary_key=True, nullable=False, server_default=text("'0'"))
    permission_id = Column(ForeignKey(u'sf_guard_permission.id', ondelete=u'CASCADE'), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    group = relationship(u'SfGuardGroup')
    permission = relationship(u'SfGuardPermission')


class SfGuardPermission(Base):
    __tablename__ = 'sf_guard_permission'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(255), unique=True)
    description = Column(Text)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class SfGuardRememberKey(Base):
    __tablename__ = 'sf_guard_remember_key'

    id = Column(BigInteger, primary_key=True)
    user_id = Column(ForeignKey(u'sf_guard_user.id', ondelete=u'CASCADE'), index=True)
    remember_key = Column(String(32))
    ip_address = Column(String(50))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    user = relationship(u'SfGuardUser')


class SfGuardUser(Base):
    __tablename__ = 'sf_guard_user'

    id = Column(BigInteger, primary_key=True)
    first_name = Column(String(255))
    last_name = Column(String(255))
    email_address = Column(String(255), nullable=False, unique=True)
    username = Column(String(128), nullable=False, unique=True)
    algorithm = Column(String(128), nullable=False, server_default=text("'sha1'"))
    salt = Column(String(128))
    password = Column(String(128))
    is_active = Column(Integer, index=True, server_default=text("'1'"))
    is_super_admin = Column(Integer, server_default=text("'0'"))
    last_login = Column(DateTime)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class SfGuardUserGroup(Base):
    __tablename__ = 'sf_guard_user_group'

    user_id = Column(ForeignKey(u'sf_guard_user.id', ondelete=u'CASCADE'), primary_key=True, nullable=False, server_default=text("'0'"))
    group_id = Column(ForeignKey(u'sf_guard_group.id', ondelete=u'CASCADE'), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    group = relationship(u'SfGuardGroup')
    user = relationship(u'SfGuardUser')


class SfGuardUserPermission(Base):
    __tablename__ = 'sf_guard_user_permission'

    user_id = Column(ForeignKey(u'sf_guard_user.id', ondelete=u'CASCADE'), primary_key=True, nullable=False, server_default=text("'0'"))
    permission_id = Column(ForeignKey(u'sf_guard_permission.id', ondelete=u'CASCADE'), primary_key=True, nullable=False, index=True, server_default=text("'0'"))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    permission = relationship(u'SfGuardPermission')
    user = relationship(u'SfGuardUser')


class Team(Base):
    __tablename__ = 'teams'

    id = Column(BigInteger, primary_key=True)
    name = Column(String(50), nullable=False)
    shorthandle = Column(String(50), nullable=False)
    flag = Column(String(2), nullable=False)
    link = Column(String(100))
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)


class TeamsInSeason(Base):
    __tablename__ = 'teams_in_seasons'

    id = Column(BigInteger, primary_key=True)
    season_id = Column(ForeignKey(u'seasons.id', ondelete=u'CASCADE'), index=True)
    team_id = Column(ForeignKey(u'teams.id', ondelete=u'CASCADE'), index=True)
    created_at = Column(DateTime, nullable=False)
    updated_at = Column(DateTime, nullable=False)

    season = relationship(u'Season')
    team = relationship(u'Team')
