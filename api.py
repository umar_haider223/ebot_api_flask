from flask import Flask, jsonify
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from tables import Match

app = Flask(__name__)

engine = create_engine('mysql+pymysql://jlindelof:J@c0B1992@ebot.tampaesports.com/ebotv3')
Session = sessionmaker(bind=engine)
session = Session()


@app.route('/')
def index():
    return 'eBot Flask API'


@app.route('/scores')
def scores():
    scores = ''
    for match in session.query(Match).order_by(Match.id.desc()).limit(10):
        scores += '%s [%s-%s] %s | ' % (match.team_a_name, match.score_a, match.score_b, match.team_b_name)
    return scores

@app.route('/matches')
def matches():
	test_data =  [
	{ teams:{
		1:{score:10,name:"Team A",players:{1:{name:"Player 1",stat:0},2:{name:"Player 1",stat:0},3:{name:"Player 1",stat:0},4:{name:"Player 1",stat:0},5:{name:"Player 1",stat:0}}},2:{score:16,name:"Team A",players:{1:{name:"Player 1",stat:0},2:{name:"Player 1",stat:0},3:{name:"Player 1",stat:0},4:{name:"Player 1",stat:0},5:{name:"Player 1",stat:0}}}}]
	return jsonify(test_data)